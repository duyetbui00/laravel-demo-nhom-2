<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|min:5|max:255|unique:products,name,".$this->id,
            "price" => "numeric|min:0",
            "amount" => "integer|min:0",
            "categories_id" => "integer|min:1",
            "file" => "image|mimes:jpeg,png,jpg,gif,svg|max:8048",
            "image" => "min:3|max:5555"
        ];
    }
}
