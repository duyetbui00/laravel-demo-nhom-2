<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProductService;
use App\Http\Requests\ProductFormRequest;

class ProductController extends Controller
{
    private $productservice, $categoryservice;

    public function __construct(ProductService $productservice, CategoryService $categoryservice)
    {
        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['list', 'create', 'edit', 'update', 'destroy', 'store']]);
        $this->middleware('permission:product-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
        $this->categoryservice = $categoryservice;
        $this->productservice = $productservice;
    }

    public function list()
    {
        $products = $this->productservice->paginate(5);
        return view("products.list")->with(["products" => $products, "categories" => $this->categoryservice->all()]);
    }

    public function create()
    {
        return response()->json(["categories" => $this->categoryservice->all()], 200);
    }

    public function store(ProductFormRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = date('YmdHis') . '.' . $image->getClientOriginalExtension();
            $image->move('uploads/products', $imageName)->getRealPath();
            $data['image'] = $imageName;
        }
        $this->productservice->create($data);
        return response()->json(["message" => "Create product success"], 201);
    }

    public function edit($id)
    {
        return response()->json([
            "categories" => $this->categoryservice->all(),
            "products" => $this->productservice->show($id),
        ], 200);
    }

    public function update(ProductFormRequest $request, $id)
    {
        if ($request->file) {
            $request["image"] = date('YmdHis') . '.' . $request->file->extension();
            $request->file->move('uploads/products', $request["image"]);
        }
        $this->productservice->update($request->all(), $id);
        return response()->json(["message" => "Update product success"], 201);
    }

    public function destroy($id)
    {
        $this->productservice->delete($id);
        return response()->json(null, 204);
    }

}
