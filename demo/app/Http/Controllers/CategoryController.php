<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryFormRequest;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    private $categoryservice;

    public function __construct(CategoryService $categoryservice)
    {
        $this->middleware('permission:category-list|category-create|category-edit|category-delete', ['only' => ['list', 'create', 'edit', 'update', 'destroy', 'store']]);
        $this->middleware('permission:category-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:category-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:category-delete', ['only' => ['destroy']]);
        $this->categoryservice = $categoryservice;
    }

    public function list()
    {
        return view("categories.list", ['categories' => $this->categoryservice->paginate(5)]);
    }

    public function create()
    {
    }

    public function store(CategoryFormRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = date('YmdHis') . '.' . $image->getClientOriginalExtension();
            $image->move('uploads/categories', $imageName)->getRealPath();
            $data['image'] = $imageName;
        }
        $this->categoryservice->create($data);
        return response()->json(['message' => 'Create category success!!'], 201);
    }

    public function show($id)
    {
        $categories = $this->category->show($id);
        return response()->json(['categories' => $categories], 200);
    }

    public function edit($id)
    {
        $categories = $this->categoryservice->show($id);
        return response()->json(['categories' => $categories], 200);
    }

    public function update(CategoryFormRequest $request, $id)
    {
        if ($request->file) {
            $request["image"] = date('YmdHis') . '.' . $request->file->extension();
            $request->file->move('uploads/categories', $request["image"]);
        }
        $this->categoryservice->update($request->all(), $id);
        return response()->json(['message' => 'Update category success!!'], 200);
    }

    public function destroy($id)
    {
        $this->categoryservice->delete($id);
        return response()->json(null, 204);
    }
}
