<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateFormRequest;
use App\Http\Requests\UserFormRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    protected $userservice, $roleservice;

    function __construct(UserService $userservice, RoleService $roleservice)
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'create', 'edit', 'update', 'destroy', 'store']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->userservice = $userservice;
        $this->roleservice = $roleservice;
    }

    public function index()
    {
       $roles = $this->roleservice->all();
        $data = $this->userservice->paginate();
        $roles_name = Role::pluck('name', 'name')->all();
        return view('users.index', compact('data', 'roles', 'roles_name'));
    }

    public function create(){
    }

    public function store(UserCreateFormRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = $this->userservice->create($input);
        $user->assignRole($request->input('roles'));
        return response()->json(['message' => 'Create user success!!'], 201);
    }

    public function show($id)
    {
        $user = $this->userservice->show($id);
        return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userservice->show($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        return response()->json([
            'user' => $user,
            'roles' => $roles,
            'userRole' => $userRole
        ], 200);
    }

    public function update(UserFormRequest $request, $id)
    {
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }
        $user = $this->userservice->show($id);
        $this->userservice->update($input,$id);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));
        return response()->json(["message" => "Update user success"], 201);
    }

    public function destroy($id)
    {
        $this->userservice->delete($id);
        return response()->json(null, 204);
    }

}
