<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository  extends BaseRepository
{
    function model()
    {
       return Product::class;
    }

    public function paginate()
    {
        return $this->model->latest()->Search()->paginate(5);
    }

}
