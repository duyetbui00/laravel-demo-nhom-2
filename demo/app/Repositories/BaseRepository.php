<?php

namespace App\Repositories;

abstract class BaseRepository
{
    protected $model;

    public function __construct()
    {
        $this->makeModel();
    }

    public function makeModel()
    {
        $this->model = app($this->model());
    }

    abstract function model();

    public function all()
    {
        return $this->model->all();
    }

    public function update( $data, $id)
    {
        return $this->model->findOrFail($id)->update($data);
    }

    public function create( $data)
    {
        return $this->model->create($data);
    }

    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

}
