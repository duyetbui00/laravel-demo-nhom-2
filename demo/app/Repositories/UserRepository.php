<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    function model()
    {
        return User::class;
    }

    public function paginate()
    {
       return $this->model->Role()->orderBy('id','DESC')->Search()->paginate(5);
    }
}
