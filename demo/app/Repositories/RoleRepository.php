<?php

namespace App\Repositories;

use App\Models\Role;


class RoleRepository  extends BaseRepository
{

     function model(){
       return Role::class;
    }

    public function paginate()
    {
        return $this->model->orderBy('id','DESC')->Search()->paginate(5);

    }

    public function create($data)
    {
      return $this->model->create(["name" => $data["name"]])->syncPermissions($data['permission']);
    }

    public function update($data, $id)
    {
        return $this->model->find($id)->syncPermissions($data['permission'])->update($data);
    }

}
