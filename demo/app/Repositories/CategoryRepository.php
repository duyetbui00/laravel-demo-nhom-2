<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository  extends BaseRepository
{
    function model()
    {
       return Category::class;
    }

    public function paginate()
    {
        return $this->model->latest()->Search()->paginate(5);
    }
}
