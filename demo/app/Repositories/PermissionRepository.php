<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission;

class PermissionRepository extends BaseRepository
{
    function model()
    {
        return Permission::class;
    }

    public function paginate()
    {
        return $this->model->orderBy('id','DESC')->Search()->paginate(5);
    }

}
