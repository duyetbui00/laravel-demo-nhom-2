<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function scopeSearch($query)
    { if($key =request()->search){
        $query=$query->where('name','like','%'.$key.'%')
                     ->Orwhere('email','like','%'.$key.'%');
    }
    }
    public function role_has_user(){
        return $this->belongsToMany(Role::class,'model_has_roles','role_id','model_id');
    }
    public function scopeRole($query)
    {
        if($key =request()->role){
            $query->with('role_has_user')->whereHas('roles', function ($query) {
                $query->where('roles.id', request()->role);
            })->get();
        }
    }
}
