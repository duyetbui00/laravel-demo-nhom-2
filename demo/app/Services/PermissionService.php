<?php

namespace App\Services;

use App\Repositories\PermissionRepository;

class PermissionService implements IService
{
    private $permissionrepo ;
    public function __construct(PermissionRepository $permissionrepo)
    {
        $this->permissionrepo=$permissionrepo;
    }

    public function paginate()
    {
        return $this->permissionrepo->paginate();
    }

    public function create($data)
    {
        return $this->permissionrepo->create($data);
    }

    public function update($data, $id)
    {
        return $this->permissionrepo->update($data,$id);
    }

    public function delete($id)
    {
        return $this->permissionrepo->delete($id);
    }

    public function show($id)
    {
        return $this->permissionrepo->show($id);
    }

    public function all()
    {
        return $this->permissionrepo->all();
    }
}
