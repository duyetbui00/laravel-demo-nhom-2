<?php

namespace App\Services;



use App\Repositories\RoleRepository;

class RoleService implements IService
{
    private $rolerepo ;
    public function __construct(RoleRepository $rolerepo)
    {
        $this->rolerepo=$rolerepo;
    }

    public function paginate()
    {
        return $this->rolerepo->paginate();
    }

    public function create($data)
    {
       return $this->rolerepo->create($data);
    }

    public function update($data, $id)
    {
        return $this->rolerepo->update($data,$id);
    }

    public function delete($id)
    {
        return $this->rolerepo->delete($id);
    }

    public function show($id)
    {
        return $this->rolerepo->show($id);
    }

    public function all()
    {
        return $this->rolerepo->all();
    }
}
