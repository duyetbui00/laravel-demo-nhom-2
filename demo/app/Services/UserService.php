<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService implements IService
{
    private $userrepo ;

    public function __construct(UserRepository $userrepo)
    {
        $this->userrepo=$userrepo;
    }

    public function paginate()
    {
        return $this->userrepo->paginate();
    }

    public function create($data)
    {
        return $this->userrepo->create($data);
    }

    public function update($data, $id)
    {
        return $this->userrepo->update($data,$id);
    }

    public function delete($id)
    {
        return $this->userrepo->delete($id);
    }

    public function show($id)
    {
        return $this->userrepo->show($id);
    }

    public function all()
    {
        return $this->userrepo->all();
    }

}
