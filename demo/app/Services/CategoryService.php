<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService implements IService
{
    private $categoryrepo;

    public function __construct(CategoryRepository $categoryrepo)
    {
        $this->categoryrepo = $categoryrepo;
    }

    public function paginate()
    {
        return $this->categoryrepo->paginate();
    }

    public function all()
    {
        return $this->categoryrepo->all();
    }

    public function show($id)
    {
        return $this->categoryrepo->show($id);
    }

    public function update($data, $id)
    {
        return $this->categoryrepo->update($data, $id);
    }

    public function create($data)
    {
        return $this->categoryrepo->create($data);
    }

    public function delete($id)
    {
        return $this->categoryrepo->delete($id);
    }
}
