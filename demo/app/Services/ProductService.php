<?php

namespace App\Services;

use App\Repositories\ProductRepository;

class ProductService implements IService
{
    private $productrepo;

    public function __construct(ProductRepository $productrepo)
    {
        $this->productrepo = $productrepo;
    }

    public function paginate()
    {
        return $this->productrepo->paginate();
    }

    public function all()
    {
        return $this->productrepo->all();
    }

    public function show($id)
    {
        return $this->productrepo->show($id);
    }

    public function update($product, $id)
    {
        return $this->productrepo->update($product, $id);
    }

    public function create($product)
    {
        return $this->productrepo->create($product);
    }

    public function delete($id)
    {
        return $this->productrepo->delete($id);
    }

}
