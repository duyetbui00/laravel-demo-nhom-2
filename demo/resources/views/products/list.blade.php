@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <h2 style="text-align:center">Product Management</h2>
            <div class="container">
                <div class=" w-100 d-flex ">
                    <form class="d-flex align-items-center flex-nowrap">
                        <input type="text" id="search" placeholder="Search by name..." class="form-control">
                        <button class="btn btn-dark" id="btn-search" type="submit"><i
                                class="fa-solid fa-magnifying-glass"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Category</th>
                <th>Image</th>
                <th>
                    @can("product-create")
                        <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#btn-create"
                           onclick="viewFromCreate()"><i class="fa-solid fa-plus"></i></a>
                    @endcan
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
                <tr>
                    <th scope="row">{{$product->id}}</th>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->amount}}</td>
                    <td>{{$product->category->name}}</td>
                    <td><img src="{{ asset("uploads/products/".$product->image) }}" class="avatar" width="50"
                             height="50"></td>
                    <th>
                        <form method="POST" action="{{ route('products.destroy', $product->id) }}">
                            @csrf
                            @can('product-edit')
                                <a data-bs-toggle="modal" data-bs-target="#btn-create"
                                   onclick="viewUpdateProduct({{ $product->id }})" class="btn btn-warning"><i
                                        class="fa-solid fa-pen"></i></a>
                            @endcan
                            @method('DELETE')
                            @can('product-delete')
                                <a data-bs-toggle="modal" data-bs-target="#btn-remove"
                                   onclick="viewDeleteProduct({{ $product->id }})"
                                   class="btn btn-danger"><i class="fa-solid fa-minus"></i></a>
                            @endcan
                        </form>
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $products->appends(request()->all())->links()}}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="btn-create" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">`
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Products</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data">
                        <div class="message"></div>
                        <div class="mb-3">
                            <label class="form-label">Name</label>
                            <input id="product-name" type="text" class="form-control">
                            <div id="product-name-message" class="valid-feedback"></div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Price</label>
                            <input id="product-price" type="number" class="form-control">
                            <div id="product-price-message" class="valid-feedback"></div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Amount</label>
                            <input id="product-amount" type="number" class="form-control">
                            <div id="product-amount-message" class="valid-feedback"></div>
                        </div>
                        <div class="mb-3">
                            <label for="product-categories" class="form-label">Category</label>
                            <select name="product-categories" id="product-categories" class="form-select" required="required">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            <div id="product-categories-message" class="valid-feedback"></div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Image</label>
                            <input id="product-file-image" type="file" class="form-control">
                            <input type="hidden" class="form-control" id="product-file">
                            <img id="product-image" style="object-fit:cover;" src="">
                            <div id="product-image-message" class="valid-feedback"></div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btn-close" data-bs-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="btn-add" onclick="createProduct()">
                        Create
                    </button>
                    <button type="button" class="btn btn-primary d-none" id="btn-update">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="btn-remove" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Product delete</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Do you want to delete it?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">No</button>
                    <button type="button" id="submit-remove" onclick="deleteProduct()"
                            class="btn btn-primary">Yes
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Search
        $(document).ready(function () {
            $("#btn-search").click(function (e) {
                e.preventDefault();
                var $value = $("#search").val();
                $.ajax({
                    type: 'get',
                    data: {
                        'search': $value,
                    },
                    url: '{{ route('products.list') }}',
                    statusCode: '200',
                    success: function (data) {
                        if ($("body").html(data)) {
                            $("table").fadeOut(10, function () {
                                $("table").fadeIn();
                            });
                        }
                    },
                });
            });
        });

        // Create
        function viewFromCreate() {
            $.ajax({
                method: 'get',
                url: '{{ route('products.create') }}',
                success: function (data) {
                    $("#exampleModalLabel").html("Create Product");
                    $("#product-name").val('').removeClass('is-invalid');
                    $("#product-amount").val('').removeClass('is-invalid');
                    $("#product-price").val('').removeClass('is-invalid');
                    $("#product-categories").val('').removeClass('is-invalid');
                    $("#product-image").val('').removeClass('is-invalid');
                    $(".alert").remove();
                    $("#btn-update").addClass("d-none").removeAttr("onclick");
                    $("#btn-add").removeClass("d-none");
                    $("#btn-add").attr("onclick", "createProduct()");
                    $("#btn-close").click(function () {
                        $("body").load("/products");
                    });
                    $("#btn-create.modal.fade.show").click(function () {
                        $("body").load("/products");
                    });
                }
            });
        }

        function createProduct() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
            $(".form-control").removeClass("is-invalid");
            $(".invalid-feedback").removeClass("invalid-feedback").addClass("valid-feedback");
            var formData = new FormData();
            formData.append('name', $("#product-name").val());
            formData.append('price', $("#product-price").val());
            formData.append('amount', $("#product-amount").val());
            formData.append('categories_id', $("#product-categories").val());
            formData.append('image', $("#product-file-image")[0].files[0]);
            console.log(formData);
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'POST',
                data: formData,
                url: '{{route('products.store')}}',
                success: function (data) {
                    Swal.fire({
                        title: 'Success!',
                        text: 'Create product success!!!',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900,
                        position: 'center',
                    });
                    setTimeout(function () {
                        $("body").load("/products");
                    }, 830);
                },
                error: function (data) {
                    validateForm(data);
                }
            });
        }

        // Update
        function viewUpdateProduct(id) {
            $("#exampleModalLabel").html("Edit Product");
            $("#btn-add").addClass("d-none").removeAttr("onclick");
            $("#btn-update").removeClass("d-none");
            $("#product-name").prop("disabled", false);
            $("#product-price").prop("disabled", false);
            $("#product-amount").prop("disabled", false);
            $("#product-categories").prop("disabled", false);
            $("#product-file-image").prop("disabled", false);
            $("#product-image").css("display", "none");
            // $("#product-file-image").val("");
            $(".alert").remove();
            $(".invalid-feedback").html("");
            $(".is-invalid").removeClass("is-invalid");
            $("#btn-close").click(function () {
                $("body").load("/products");
            });
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/products/edit/' + id,
                success: function (data) {
                    console.log(data);
                    $("#product-name").val(data.products.name);
                    $("#product-price").val(data.products.price);
                    $("#product-amount").val(data.products.amount);
                    $("#product-file").val(data.products.image);
                    $("#btn-update").removeClass("d-none").attr("onclick", "updateProduct(" + id + ")");
                    var option = '';
                    $.each(data.categories, function (i) {
                        if (data.categories[i].id == data.products.categories_id) {
                            option += '<option value="' + data.categories[i].id + '" selected >' +
                                data.categories[i].name + '</option>'
                        } else {
                            option += '<option value="' + data.categories[i].id + '">' + data
                                .categories[i].name + '</option>';
                        }
                    });
                    $("#product-categories").html(option);
                }
            });
        }

        function updateProduct(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
            var formData = new FormData();
            formDataProduct(formData);
            formData.append('_method', 'PUT');
            formData.append('id', id);
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '/products/update/' + id,
                success: function (data) {
                    $(".form-control").removeClass("is-invalid");
                    $(".invalid-feedback").removeClass("invalid-feedback").addClass("valid-feedback");
                    Swal.fire({
                        title: 'Success!',
                        text: 'Update product success',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900,
                        position: 'center',
                    });
                    setTimeout(function () {
                        $("body").load("/products");
                    }, 830);
                },
                error: function (data) {
                    validateForm(data);
                }
            });
        }

        // Delete
        function viewDeleteProduct(id) {
            $("#submit-remove").val(id);
        }

        function deleteProduct() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $("#submit-remove").val();
            $.ajax({
                type: "delete",
                url: "/products/destroy/" + id,
                success: function (data) {
                    window.location.reload();
                }
            })
        }

        function formDataProduct(formData) {
            if ($("#product-file-image").prop('files')[0]) {
                formData.append('file', $("#product-file-image").prop('files')[0]);
                formData.append('image', $("#product-file").val());
            } else {
                formData.append('image', $("#product-file").val());
            }
            formData.append('name', $("#product-name").val());
            formData.append('price', $("#product-price").val());
            formData.append('amount', $("#product-amount").val());
            formData.append('categories_id', $("#product-categories").val());
            return formData;
        }

        function validateForm(response) {
            $(".message").html('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                response.responseJSON.message +
                '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                '</div>')
            if (response.responseJSON.errors.name) {
                $("#product-name").addClass("is-invalid");
                $("#product-name-message").html(response.responseJSON.errors.name)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (response.responseJSON.errors.image) {
                $("#product-file-image").addClass("is-invalid");
                $("#product-image-message").html(response.responseJSON.errors.image)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (response.responseJSON.errors.price) {
                $("#product-price").addClass("is-invalid");
                $("#product-price-message").html(response.responseJSON.errors.price)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (response.responseJSON.errors.amount) {
                $("#product-amount").addClass("is-invalid");
                $("#product-amount-message").html(response.responseJSON.errors.amount)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (response.responseJSON.errors.categories_id) {
                $("#product-categories").addClass("is-invalid");
                $("#product-categories-message").html(response.responseJSON.errors.categories_id)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
        }

    </script>

@endsection
