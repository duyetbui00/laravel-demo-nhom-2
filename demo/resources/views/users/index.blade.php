@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h2 style="text-align: center">User Management</h2>
            <br>
            <div class="container row">
                <div class="col-4">
                    <form class="d-flex align-items-center flex-nowrap">
                        <input type="text" id="search" placeholder="Search by name..." class="form-control">
                        <button class="btn btn-dark" id="btn-search" type="submit"><i
                                class="fa-solid fa-magnifying-glass"></i></button>
                    </form>
                </div>
                <div class="col-4">
                    <form id='role' action="{{route('users.index')}}">
                        <select name='role' onchange="document.getElementById('role').submit()" class="form-select"
                                aria-label="Default select example">
                            <option value="" selected>Role</option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name}}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <table class="table table-bordered table-striped">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th width="280px">
                    <div class="pull-right">
                        @can('user-create')
                            <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#btn-create"
                               onclick="viewFromCreate()"><i class="fa-solid fa-plus"></i></a>
                        @endcan
                    </div>
                </th>
            </tr>
            @foreach ($data as $key => $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                                <label class="badge badge-success text-success">{{ $v }}</label>
                            @endforeach
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-info" href="{{ route('users.show',$user->id) }}"><i
                                class="fa-solid fa-eye"></i></a>
                        @can('user-edit')
                            <a data-bs-toggle="modal" data-bs-target="#btn-create"
                               onclick="viewUpdateUser({{ $user->id }})"
                               class="btn btn-warning"><i class="fa-solid fa-pen"></i></a>
                        @endcan
                        @can('user-delete')
                            <a data-bs-toggle="modal" data-bs-target="#btn-remove"
                               onclick="viewDeleteUser({{ $user->id }})"
                               class="btn btn-danger"><i class="fa-solid fa-minus"></i></a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $data->appends(request()->all())->links()}}

        {{-- modal-create-update --}}
        <div class="modal fade" id="btn-create" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">`
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form enctype="multipart/form-data">
                            <div class="message"></div>
                            <div class="mb-3">
                                <label class="form-label">Name</label>
                                <input id="user-name" type="text" class="form-control">
                                <div id="user-name-message" class="valid-feedback"></div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input id="user-email" type="text" class="form-control">
                                <div id="user-email-message" class="valid-feedback"></div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Password</label>
                                <input id="user-password" type="password" class="form-control">
                                <div id="user-password-message" class="valid-feedback"></div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Confirm Password</label>
                                <input id="user-confirm" type="password" class="form-control">
                                <div id="user-confirm-message" class="valid-feedback"></div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Role</label>
                                {!! Form::select('roles[]', $roles_name ,[], array('class' => 'form-control','multiple','id' => 'user-role')) !!}
                                <div id="user-role-message" class="valid-feedback"></div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="btn-close" data-bs-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary" id="btn-add" onclick="createUser()">
                            Add
                        </button>
                        <button type="button" class="btn btn-primary d-none" id="btn-update">Save</button>
                    </div>
                </div>
            </div>
        </div>

        {{--    modal-delete--}}
        <div class="modal fade" id="btn-remove" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">User delete</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Do you want to delete it?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">No</button>
                        <button type="button" id="submit-remove" onclick="deleteUser()"
                                class="btn btn-primary">Yes
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" charset="utf-8">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //Search
        $(document).ready(function () {
            $("#btn-search").click(function (e) {
                e.preventDefault();
                var $value = $("#search").val();
                $.ajax({
                    type: 'get',
                    data: {
                        'search': $value,
                    },
                    url: '{{ route('users.index') }}',
                    statusCode: '200',
                    success: function (data) {
                        if ($("body").html(data)) {
                            $("table").fadeOut(10, function () {
                                $("table").fadeIn();
                            });
                        }
                    },
                });
            });
        });

        //Create
        function viewFromCreate() {
            $.ajax({
                method: 'get',
                url: '{{ route('users.create') }}',
                success: function (data) {
                    $("#exampleModalLabel").html("Create user");
                    $("#user-name").val('').removeClass('is-invalid');
                    $("#user-email").val('').removeClass('is-invalid');
                    $("#user-password").val('').removeClass('is-invalid');
                    $("#user-confirm").val('').removeClass('is-invalid');
                    $("#user-role").val('').removeClass('is-invalid');
                    $(".alert").remove();
                    $("#btn-update").addClass("d-none").removeAttr("onclick");
                    $("#btn-add").removeClass("d-none");
                    $("#btn-add").attr("onclick", "createUser()");
                    $("#btn-close").click(function () {
                        $("body").load("/users");
                    });
                    $("#btn-create.modal.fade.show").click(function () {
                        $("body").load("/users");
                    });
                }
            });
        }

        function createUser() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            formDataUser(formData);
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '{{ route('users.store') }}',
                success: function (data) {
                    Swal.fire({
                        title: 'Success!',
                        text: 'Create user success!!!',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900,
                        position: 'center',
                    });
                    setTimeout(function () {
                        $("body").load("/users");
                    }, 830);
                },
                error: function (data) {
                    validateForm(data);
                }
            });
        }

        // Update
        function viewUpdateUser(id) {
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/users/edit/' + id,
                success: function (data) {
                    $("#exampleModalLabel").html("Edit User");
                    $("#user-name").val(data.user.name).removeClass('is-invalid');
                    $("#user-email").val(data.user.email).removeClass('is-invalid');
                    $("#user-password").val("").removeClass('is-invalid');
                    $("#user-confirm").val("").removeClass('is-invalid');
                    $(".alert").remove();
                    $("#btn-add").addClass("d-none").removeAttr("onclick");
                    $("#btn-update").removeClass("d-none").attr("onclick", "updateUsers(" + id + ")");
                    $("#btn-close").click(function () {
                        $("body").load("/users");
                    });
                    $("#btn-create.modal.fade.show").click(function () {
                        $("body").load("/users");
                    });
                    var option = '';
                    var optionval = [];
                    $.each(data.roles, function (i) {
                        optionval[i] = '<option value="' + data.roles[i] + '">' + data.roles[i] +
                            '</option>';
                    });
                    $.each(data.userRole, function (j) {
                        $.each(data.roles, function (i) {
                            if (data.roles[i] == data.userRole[j]) {
                                optionval[i] = '<option value="' + data.roles[i] +
                                    '" selected >' + data.roles[i] + '</option>';
                            }
                        });
                    });
                    $.each(data.roles, function (i) {
                        option += optionval[data.roles[i]];
                    });
                    $("#user-role").html(option).removeClass('is-invalid');
                    // console.log(optionval);
                }
            });
        }

        function updateUsers(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            formData.append('_method', 'PUT');
            formData.append('id', id);
            formDataUser(formData);
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '/users/update/' + id,
                success: function (data) {
                    $(".form-control").removeClass("is-invalid");
                    $(".invalid-feedback").removeClass("invalid-feedback").addClass("valid-feedback");
                    Swal.fire({
                        title: 'Success!',
                        text: 'Update user success',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900,
                        position: 'center',
                    });
                    setTimeout(function () {
                        $("body").load("/users");
                    }, 830);
                },
                error: function (data) {
                    validateForm(data);
                }
            });
        }

        // Delete
        function viewDeleteUser(id) {
            $("#submit-remove").val(id);
        }

        function deleteUser() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $("#submit-remove").val();
            $.ajax({
                type: "delete",
                url: "/users/destroy/" + id,
                success: function (data) {
                    window.location.reload();
                }
            })
        }

        // formdata
        function formDataUser(formData) {
            formData.append('name', $("#user-name").val());
            formData.append('email', $("#user-email").val());
            formData.append('password', $("#user-password").val());
            formData.append('confirm-password', $("#user-confirm").val());
            var arr = [];
            $('#user-role :selected').each(function (i, role) {
                arr.push($(role).val());
            });
            for (var i = 0; i < arr.length; i++) {
                formData.append('roles[]', arr[i]);
            }
        }

        // Validate
        function validateForm(data) {
            $(".message").html('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                data.responseJSON.message +
                '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                '</div>')
            if (data.responseJSON.errors.name) {
                $("#user-name").addClass("is-invalid");
                $("#user-name-message").html(data.responseJSON.errors.name)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (data.responseJSON.errors.email) {
                $("#user-email").addClass("is-invalid");
                $("#user-email-message").html(data.responseJSON.errors.email)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (data.responseJSON.errors.password) {
                $("#user-password").addClass("is-invalid");
                $("#user-password-message").html(data.responseJSON.errors.password)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (data.responseJSON.errors.confirm) {
                $("#user-confirm").addClass("is-invalid");
                $("#user-confirm-message").html(data.responseJSON.errors.confirm)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (data.responseJSON.errors.roles) {
                $("#user-role").addClass("is-invalid");
                $("#user-role-message").html(data.responseJSON.errors.roles)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
        }
    </script>
@endsection
