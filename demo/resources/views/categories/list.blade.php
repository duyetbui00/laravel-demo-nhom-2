@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <h2 style="text-align:center">Category Management</h2>
            <div class="container">
                <div class=" w-100 d-flex ">
                    <form class="d-flex align-items-center flex-nowrap">
                        <input type="text" id="search" placeholder="Search by name..." class="form-control">
                        <button class="btn btn-dark" id="btn-search" type="submit"><i
                                class="fa-solid fa-magnifying-glass"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <table class="table table-striped table-bordered">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Date Created</th>
                <th>Date Updated</th>
                <th>Status</th>
                <th> @can('category-create')
                        <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#btn-create"
                           onclick="viewFromCreate()"><i class="fa-solid fa-plus"></i></a>
                    @endcan
                </th>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <th>{{ $category->id }}</th>
                    <td><a href="#"><img src="{{ asset('uploads/categories/' . $category->image)}}" class="avatar"
                                         alt="" width="50" height="50"> {{ $category->name }}</a></td>
                    <td>{{ $category->created_at }}</td>
                    <td>{{ $category->updated_at }}</td>
                    @if($category->status)
                        <td><span class="status text-success">&bull;</span> Active</td>
                    @else
                        <td><span class="status text-danger">&bull;</span> Disable</td>
                    @endif
                    <th>
                        <form method="POST" action="{{ route('categories.destroy', $category->id) }}">
                            @csrf
                            @can('category-edit')
                                <a data-bs-toggle="modal" data-bs-target="#btn-create"
                                   onclick="viewUpdateCategories({{ $category->id }})" class="btn btn-warning"><i
                                        class="fa-solid fa-pen"></i></a>
                            @endcan
                            @method('DELETE')
                            @can('category-delete')
                                <a data-bs-toggle="modal" data-bs-target="#btn-remove"
                                   onclick="viewDeleteCategories({{ $category->id }})"
                                   class="btn btn-danger"><i class="fa-solid fa-minus"></i></a>
                            @endcan
                        </form>
                    </th>
                </tr>
            @endforeach
        </table>
        {{ $categories->appends(request()->all())->links()}}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="btn-create" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">`
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New Categories</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data">
                        <div class="message"></div>
                        <div class="mb-3">
                            <label class="form-label">Name</label>
                            <input id="category-name" type="text" class="form-control">
                            <div id="category-name-message" class="valid-feedback"></div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Image</label>
                            <input id="category-file-image" type="file" class="form-control">
                            <input type="hidden" class="form-control" id="category-file">
                            <img id="category-img" style="object-fit:cover;" src="">
                            <div id="category-image-message" class="valid-feedback"></div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Status</label>
                            <select class="form-control" id="category-status" name="status">
                                <option value="1">Active</option>
                                <option value="0">Disable</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btn-close" data-bs-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="btn-add" onclick="createCategory()">
                        Create
                    </button>
                    <button type="button" class="btn btn-primary d-none" id="btn-update">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="btn-remove" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Category delete</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Do you want to delete it?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">No</button>
                    <button type="button" id="submit-remove" onclick="deleteCategories()"
                            class="btn btn-primary">Yes
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //Search
        $(document).ready(function () {
            $("#btn-search").click(function (e) {
                e.preventDefault();
                var $value = $("#search").val();
                $.ajax({
                    type: 'get',
                    data: {
                        'search': $value,
                    },
                    url: '{{ route('categories.list') }}',
                    statusCode: '200',
                    success: function (data) {
                        if ($("body").html(data)) {
                            $("table").fadeOut(10, function () {
                                $("table").fadeIn();
                            });
                        }
                    },
                });
            });
        });

        // Create
        function viewFromCreate() {
            $.ajax({
                method: 'get',
                url: '{{ route('categories.create') }}',
                success: function (data) {
                    $("#exampleModalLabel").html("Create Category");
                    $("#category-name").val('').removeClass('is-invalid');
                    $("#category-file-image").val('').removeClass('is-invalid');
                    $("#category-img").val('').removeClass('is-invalid');
                    $(".alert").remove();
                    $("#btn-update").addClass("d-none").removeAttr("onclick");
                    $("#btn-add").removeClass("d-none");
                    $("#btn-add").attr("onclick", "createCategory()");
                    $("#btn-close").click(function () {
                        $("body").load("/categories");
                    });
                    $("#btn-create.modal.fade.show").click(function () {
                        $("body").load("/categories");
                    });
                }
            });
        }

        function createCategory() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var formData = new FormData();
            formData.append('name', $("#category-name").val());
            formData.append('image', $("#category-file-image")[0].files[0]);
            formData.append('status', $("#category-status").val());
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '{{ route('categories.store') }}',
                success: function (data) {
                    Swal.fire({
                        title: 'Success!',
                        text: 'Create categories success!!!',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900,
                        position: 'center',
                    });
                    setTimeout(function () {
                        $("body").load("/categories");
                    }, 830);
                },
                error: function (data) {
                    validateForm(data);
                }
            });
        }

        function formDataCategory(form) {
            if ($("#category-file-image").prop('files')[0]) {
                form.append('file', $("#category-file-image").prop('files')[0]);
                form.append('image', $("#category-file").val());
            } else {
                form.append('image', $("#category-file").val());
            }
            form.append('name', $("#category-name").val());
            form.append('status', $("#category-status").val());
            return form;
        }

        function validateForm(response) {
            $(".message").html('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                response.responseJSON.message +
                '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                '</div>')
            if (response.responseJSON.errors.name) {
                $("#category-name").addClass("is-invalid");
                $("#category-name-message").html(response.responseJSON.errors.name)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
            if (response.responseJSON.errors.image) {
                $("#category-file-image").addClass("is-invalid");
                $("#category-image-message").html(response.responseJSON.errors.image)
                    .removeClass("valid-feedback").addClass("invalid-feedback");
            }
        }

        // Update
        function viewUpdateCategories(id) {
            $("#exampleModalLabel").html("Edit category");
            $("#btn-add").addClass("d-none").removeAttr("onclick");
            $("#btn-update").removeClass("d-none");
            $("#category-name").prop("disabled", false);
            $("#category-status").prop("disabled", false);
            $("#category-file-image").prop("disabled", false);
            $("#category-img").css("display", "none");
            $("#category-file-image").val("");
            $(".alert").remove();
            $(".invalid-feedback").html("");
            $(".is-invalid").removeClass("is-invalid");
            $("#btn-close").click(function () {
                $("body").load("/categories");
            });

            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/categories/edit/' + id,
                success: function (data) {
                    console.log(data);
                    $("#category-name").val(data.categories.name);
                    $("#category-file").val(data.categories.image);
                    $("#category-status").val(data.categories.status);
                    $("#btn-update").removeClass("d-none").attr("onclick", "updateCategories(" + id + ")");
                }
            });
        }

        function updateCategories(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                }
            });
            var formData = new FormData();
            formData.append('_method', 'PUT');
            formData.append('id', id);
            formDataCategory(formData);
            console.log(formData);
            $.ajax({
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'post',
                data: formData,
                url: '/categories/update/' + id,
                success: function (data) {
                    $(".form-control").removeClass("is-invalid");
                    $(".invalid-feedback").removeClass("invalid-feedback").addClass("valid-feedback");
                    Swal.fire({
                        title: 'Success!',
                        text: 'Update category success',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 900,
                        position: 'center',
                    });
                    setTimeout(function () {
                        $("body").load("/categories");
                    }, 830);
                },
                error: function (data) {
                    validateForm(data);
                }
            });
        }

        // Delete
        function viewDeleteCategories(id) {
            $("#submit-remove").val(id);
        }

        function deleteCategories() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $("#submit-remove").val();
            $.ajax({
                type: "delete",
                url: "/categories/destroy/" + id,
                success: function (data) {
                    window.location.reload();
                }
            })
        }

    </script>

@endsection
