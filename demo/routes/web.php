<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products', [ProductController::class,'list'])->name('products.list')
    ->middleware('auth');

Route::get('/products/create', [ProductController::class,'create'])->name('products.create')
    ->middleware('auth');

Route::post('/products', [ProductController::class,'store'])->name('products.store')
    ->middleware('auth');

Route::get('/products/edit/{id}', [ProductController::class,'edit'])->name('products.edit')
    ->middleware('auth');

Route::put('/products/update/{id}', [ProductController::class,'update'])->name('products.update')
    ->middleware('auth');

Route::delete('/products/destroy/{id}', [ProductController::class,'destroy'])->name('products.destroy')
    ->middleware('auth');

Route::get('/categories', [CategoryController::class,'list'])->name('categories.list')
    ->middleware('auth');

Route::get('/categories/create', [CategoryController::class,'create'])->name('categories.create')
    ->middleware('auth');

Route::post('/categories', [CategoryController::class,'store'])->name('categories.store')
    ->middleware('auth');

Route::get('/categories/edit/{id}', [CategoryController::class,'edit'])->name('categories.edit')
    ->middleware('auth');

Route::put('/categories/update/{id}', [CategoryController::class,'update'])->name('categories.update')
    ->middleware('auth');

Route::delete('/categories/destroy/{id}', [CategoryController::class,'destroy'])->name('categories.destroy')
    ->middleware('auth');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::get("/users", [UserController::class, "index"])->name("users.index");
    Route::post("/users", [UserController::class, "store"])->name("users.store");
    Route::get("/users/create", [UserController::class, "create"])->name("users.create");
    Route::get("/users/edit/{id}", [UserController::class, "edit"])->name("users.edit");
    Route::put("/users/update/{id}", [UserController::class, "update"])->name("users.update");
    Route::delete("/users/destroy/{id}", [UserController::class, "destroy"])->name("users.destroy");
    Route::get("/users/show/{id}", [UserController::class, "show"])->name("users.show");
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

