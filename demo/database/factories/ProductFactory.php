<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->name,
            "image" => $this->faker->image,
            "price" => $this->faker->numberBetween(0,9000000),
            "amount" => $this->faker->numberBetween(0,999),
            "categories_id" => $this->faker->numberBetween(10,20)

        ];
    }
}
