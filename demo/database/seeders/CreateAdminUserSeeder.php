<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

        public function run()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'Admin@gmail.com',
            'password' => bcrypt('123456')
        ]);

        $manager = User::create([
            'name' => 'Manager',
            'email' => 'Manager@gmail.com',
            'password' => bcrypt('123456')
        ]);
        // create user
        $roleadmin = Role::create(['name' => 'Admin']);
        $rolemanager = Role::create(['name' => 'Manager']);

        //grant permission admin
        $permissions = Permission::pluck('id', 'id')->all();
        $roleadmin->syncPermissions($permissions);
        $admin->syncPermissions($permissions);
        $admin->assignRole([$roleadmin->id]);

        //grant permission manager
        $rolemanager->givePermissionTo('user-list');
        $rolemanager->givePermissionTo('product-list');
        $rolemanager->givePermissionTo('product-create');
        $rolemanager->givePermissionTo('product-edit');
        $rolemanager->givePermissionTo('product-delete');
//        $rolemanager->givePermissionTo('category-list');
//        $rolemanager->givePermissionTo('category-create');
//        $rolemanager->givePermissionTo('category-edit');
//        $rolemanager->givePermissionTo('category-delete');
        $manager->assignRole([$rolemanager->id]);

        //verify email
        $admin->markEmailAsVerified();
        $manager->markEmailAsVerified();
    }
}
