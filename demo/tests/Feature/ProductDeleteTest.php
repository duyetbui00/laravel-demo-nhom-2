<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductDeleteTest extends TestCase
{
    public function getDeleteProductRoute($id)
    {
        return route('products.destroy',$id);
    }

    public function test_unauth_can_not_delete_user()
    {
        $products = Product::factory()->create();
        $response=$this->delete($this->getDeleteProductRoute($products->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_not_admin_can_not_delete_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $products = Product::factory()->create();
        $response=$this->delete($this->getDeleteProductRoute($products->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_delete_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo("product-delete");
        $products = Product::factory()->create();
        $response=$this->delete($this->getDeleteProductRoute($products->id));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

}
