<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductCreateTest extends TestCase
{
    public function getProductCreateRoute()
    {
        return route("products.store");
    }

    public function getProductCreateViewRoute()
    {
        return route("products.create");
    }

    public function test_unauth_can_not_view_create_product()
    {
        $response =$this->get($this->getProductCreateViewRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_not_admin_can_not_view_create_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $response =$this->get($this->getProductCreateViewRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_view_create_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo("product-create");
        $response =$this->get($this->getProductCreateViewRoute());
        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_unauth_can_not_create_product()
    {
        $response =$this->post($this->getProductCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_not_admin_can_not_create_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $response =$this->post($this->getProductCreateRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_create_product()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $user->givePermissionTo("product-create");
        $products = Product::factory()->make();
        $response = $this->post($this->getProductCreateRoute(), $products->toArray());
        $response->assertStatus(Response::HTTP_CREATED)->assertSeeText("Create product success");
    }

    public function test_admin_can_see_validate_create_product()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $user->givePermissionTo("product-create");
        $products = Product::factory()->make();
        $products->name =null;
        $response = $this->post($this->getProductCreateRoute(), $products->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

}
