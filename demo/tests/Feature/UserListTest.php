<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserListTest extends TestCase
{
    public function getListUserRoute()
    {
        return route('users.index');
    }

    public function test_admin_can_view_list_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo('user-list');
        $response =$this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
    }

    public function test_unadmin_can_not_view_list_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $response =$this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);

    }

    public function test_unauth_can_not_view_list_user()
    {

        $response =$this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');

    }

}
