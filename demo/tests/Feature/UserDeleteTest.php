<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserDeleteTest extends TestCase
{
    public function getDeleteUserRoute($id)
    {
        return route('users.destroy',$id);
    }

    public function test_admin_can_delete_user()
    {
        $user=User::factory()->create();
        $admin = $this->actingAs($user);
        $user->givePermissionTo('user-delete');
        $users = User::factory()->create();
        $response=$this->delete($this->getDeleteUserRoute($users->id));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    public function test_unadmin_can_not_delete_user()
    {
        $user=User::factory()->create();
        $admin = $this->actingAs($user);
        $users = User::factory()->create();
        $response=$this->delete($this->getDeleteUserRoute($users->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauth_can_not_delete_user()
    {
        $users = User::factory()->create();
        $response=$this->delete($this->getDeleteUserRoute($users->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
