<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleDeleteTest extends TestCase
{

    public function getDeleteRoleRoute($id)
    {
        return route('roles.destroy',$id);
    }

    public function test_auth_is_admin_can_delete_role()
    {
        $user = User::where('email' , 'Admin@gmail.com')->first();
        $this->actingAs($user);
        $role=Role::factory()->create();
        $user->assignRole(1);
        $response=$this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function test_auth_not_admin_can_not_delete_role()
    {
        $user =User::factory()->create();
        $this->actingAs($user);
        $role=Role::factory()->create();
        $user->assignRole(3);
        $response=$this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauth_can_not_delete_role()
    {
        $role=Role::factory()->create();
        $response=$this->delete($this->getDeleteRoleRoute($role->id));
        $response->assertRedirect('/login');
    }

}
