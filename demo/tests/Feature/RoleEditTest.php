<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleEditTest extends TestCase
{
    public function getEditRoleViewRoute($id)
    {
        return route('roles.edit',$id);
    }

    public function getEditRoleRoute($id)
    {
        return route('roles.update',$id);
    }

    public function test_admin_can_update_role()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $user->assignRole(1);
        $roles = Role::factory()->create();
        $response = $this->put($this->getEditRoleRoute($roles->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');
    }

    public function test_unadmin_can_not_update_role()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $roles = Role::factory()->create();
        $response = $this->put($this->getEditRoleRoute($roles->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauthenticate_can_not_update_role()
    {
        $user = User::factory()->create();
        $roles = Role::factory()->create();
        $response = $this->put($this->getEditRoleRoute($roles->id));
        $response->assertRedirect('/login');
    }

    public function test_admin_can_see_validate_update_role()
    {
        $user = User::factory()->create();
        $user->assignRole(1);
        $admin = $this->actingAs($user);

        $roles = Role::factory()->create();
        $roles->name = null;
        $response = $this->from($this->getEditRoleViewRoute($roles->id))->put($this->getEditRoleRoute($roles->id));
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

    public function test_admin_can_see_update_role()
    {
        $user = User::factory()->create();
        $user->assignRole(1);
        $admin = $this->actingAs($user);
        $roles = Role::factory()->create();
        $response = $this->get($this->getEditRoleViewRoute($roles->id));
        $response->assertViewIs('roles.edit');
    }

    public function test_unadmin_can_not_see_update_role()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $roles = Role::factory()->create();
        $response = $this->get($this->getEditRoleViewRoute($roles->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);

    }

    public function test_unauthenticate_can_not_see_update_role()
    {
        $user = User::factory()->create();
        $roles = Role::factory()->create();
        $response = $this->get($this->getEditRoleViewRoute($roles->id));
        $response->assertRedirect('/login');
    }

}
