<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductEditTest extends TestCase
{
    public function getProductEditRoute($id)
    {
        return route("products.update",$id);
    }

    public function getProductEditViewRoute($id)
    {
        return route("products.edit",$id);
    }

    public function test_unauth_can_not_see_view_update_product()
    {
        $products = Product::factory()->create();
        $response = $this->get($this->getProductEditViewRoute($products->id));
        $response->assertStatus(Response::HTTP_FOUND)->assertRedirect("login");
    }

    public function test_not_admin_can_not_see_view_update_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $products = Product::factory()->create();
        $response = $this->get($this->getProductEditViewRoute($products->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_see_view_update_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionto("product-edit");
        $products = Product::factory()->create();
        $response = $this->get($this->getProductEditViewRoute($products->id));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_unauth_can_not_update_product()
    {
        $products = Product::factory()->create();
        $response = $this->put($this->getProductEditRoute($products->id),$products->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertRedirect("login");
    }

    public function test_not_admin_can_not_update_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $products = Product::factory()->create();
        $response = $this->put($this->getProductEditRoute($products->id),$products->toArray());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_update_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionto("product-edit");
        $products = Product::factory()->create();
        $response = $this->put($this->getProductEditRoute($products->id),$products->toArray());
        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function test_admin_can_see_validate_update_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionto("product-edit");
        $products= Product::factory()->create();
        $response = $this->put($this->getProductEditRoute($products->id), [
            'name' => null,
        ]);
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

}
