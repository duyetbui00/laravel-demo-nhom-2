<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Category;
use App\Models\User;

class CategoryEditTest extends TestCase
{
    public function getViewCategoryEditRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getCategoryEditRoute($id)
    {
        return route('categories.update', $id);
    }

    /** @test  */
    public function authenticated_user_can_not_view_category_update_form()
    {
        $categories = Category::factory()->create();
        $response = $this->get($this->getViewCategoryEditRoute($categories->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_can_view_category_update_form()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-edit");
        $categories = Category::factory()->create();
        $response = $this->get($this->getViewCategoryEditRoute($categories->id));
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_user_can_update_category()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-edit");
        $categories = Category::factory()->create();
        $response = $this->put($this->getCategoryEditRoute($categories->id), $categories->toArray());
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $categories = Category::factory()->create();
        $response = $this->put($this->getCategoryEditRoute($categories->id), $categories->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_update_category_when_fields_null()
    {
        $this->actingAs(User::factory()->create()->givePermissionTo("category-edit"));
        $categories = Category::factory()->create();
        $categories->name = null;
        $response = $this->put($this->getCategoryEditRoute($categories->id), $categories->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticate_user_can_not_update_when_validation_form()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-edit");
        $categories = Category::factory()->create();
        $categories->name = null;
        $response = $this->put($this->getCategoryEditRoute($categories->id), $categories->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }
}
