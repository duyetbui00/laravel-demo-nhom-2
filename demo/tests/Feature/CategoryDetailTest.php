<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\User;
use App\Models\Category;

class CategoryDetailTest extends TestCase
{
    public function getViewShowCategory($id)
    {
        return route('categories.destroy', $id);
    }

    /** @test */
    public function authenticated_user_can_view_details_category_form()
    {
        $this->actingAs(User::factory()->create());
        $categories = Category::factory()->create();
        $response = $this->get($this->getViewShowCategory($categories->id));
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /** @test */
    public function authenticated_user_cannot_view_details_category_form()
    {
        $categories = Category::factory()->create();
        $response = $this->get($this->getViewShowCategory($categories->id));
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }
}
