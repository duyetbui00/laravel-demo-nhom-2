<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserCreateTest extends TestCase
{
    public function getCreateUserViewRoute()
    {
        return route('users.create');
    }

    public function getCreateUserRoute()
    {
        return route('users.store');
    }

    public function test_admin_can_view_create_user()
    {
        $admin = $this->actingAs(User::factory()->create()->givePermissionTo("user-create"));
        $response = $this->get($this->getCreateUserViewRoute());
        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_unadmin_can_not_view_create_user()
    {
        $admin = $this->actingAs( User::factory()->create());
        $response = $this->get($this->getCreateUserViewRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauth_can_not_view_create_user()
    {
        $response =$this->get($this->getCreateUserViewRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_unauth_can_not_create_user()
    {
        $response =$this->post($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_unadmin_can_not_create_user()
    {
        $user = User::factory()->create();
        $users = User::factory()->make();
        $admin = $this->actingAs($user);
        $response = $this->post($this->getCreateUserRoute(),$users->toArray());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_create_user()
    {
        $user = User::factory()->create();
        $users = User::factory()->make();
        $admin = $this->actingAs($user);
        $user->givePermissionTo("user-create");
        $response = $this->post($this->getCreateUserRoute(),['name' => $users->name,
            'email' =>  $users->email,
            'password' => $users->password,
            'confirm-password' => $users->password,
            'roles' => 'Admin']);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertSeeText("Create user success!");
    }

    public function test_admin_can_see_validate_create_user()
    {
        $user = User::factory()->create();
        $users = User::factory()->make();
        $admin = $this->actingAs($user);
        $user->givePermissionTo("user-create");
        $users ->name =null;
        $response = $this->post($this->getCreateUserRoute(), $users->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

}
