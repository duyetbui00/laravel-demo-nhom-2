<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\Role;

class RoleCreateTest extends TestCase
{
    public function getCreateRoleViewRoute()
    {
        return route('roles.create');
    }

    public function getCreateRoleRoute()
    {
        return route('roles.store');
    }

    public function test_admin_can_view_create_role()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $user -> assignRole(1);
        $response = $this->get($this->getCreateRoleViewRoute());
        $response->assertViewIs('roles.create');
    }

    public function test_unadmin_can_not_view_create_role()
    {
        $user = User::factory()->create();
        $admin = $this->actingAs($user);
        $response = $this->get($this->getCreateRoleViewRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauthenticate_can_not_view_create_role()
    {
        $response = $this->get($this->getCreateRoleViewRoute());
        $response->assertRedirect('/login');
    }

    public function test_admin_can_create_role()
    {
        $user =User::factory()->create();
        $this->actingAs($user);
        $user->assignRole(1);
        $response=$this->post($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');

    }

    public function test_unadmin_can_not_create_role()
    {
        $admin = $this->actingAs(User::factory()->create());
        $roles = Role::factory()->create();
        $response = $this->post($this->getCreateRoleRoute(), $roles->toArray());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauthenticate_can_not_create_role()
    {
        $roles = Role::factory()->create();
        $response = $this->post($this->getCreateRoleRoute(), $roles->toArray());
        $response->assertRedirect('/login');
    }

    public function test_auth_user_can_see_validate_create_role()
    {
        $user = User::factory()->create();
        $user -> assignRole(1);
        $admin = $this->actingAs($user);
        $roles = Role::factory()->create();
        $roles ->name =null;
        $response = $this->post($this->getCreateRoleRoute(), $roles->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

}
