<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleListTest extends TestCase
{
    public function getListRoleRoute()
    {
        return route('roles.index');
   }
    public function test_admin_user_can_view_list_role()
    {
     $user =User::factory()->create();
     $admin=$this->actingAs($user);
     $user->assignRole(1);
     $response=$this->get($this->getListRoleRoute());
     $response->assertStatus(Response::HTTP_OK);
     $response->assertViewIs('roles.index');
    }

    public function test_unauth_can_not_view_list_role()
    {
        $response =$this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
    public function test_unadmin_can_not_view_list_role()
    {
        $admin = $this->actingAs(User::factory()->create());
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

}
