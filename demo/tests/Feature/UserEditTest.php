<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserEditTest extends TestCase
{
    public function getEditUserViewRoute($id)
    {
        return route('users.edit',$id);
    }

    public function getEditUserRoute($id)
    {
        return route('users.update',$id);
    }

    public function test_admin_can_see_view_update_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo('user-edit');
        $users = User::factory()->create();
        $response = $this->get($this->getEditUserViewRoute($users->id));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_unadmin_can_not_see_view_update_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $users = User::factory()->create();
        $response = $this->get($this->getEditUserViewRoute($users->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauth_can_see_not_view_update_user()
    {
        $users = User::factory()->create();
        $response = $this->get($this->getEditUserViewRoute($users->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_admin_can_update_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo('user-edit');
        $users = User::factory()->create();
        $response = $this->put($this->getEditUserRoute($users->id), [
            'name' => $users->name,
            'email' =>  $users->email,
            'password-password' => $users->password,
            'confirm' => $users->password,
            'roles' => 'Admin'
        ]);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertSeeText("Update user success");
    }

    public function test_unadmin_can_not_update_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $users = User::factory()->create();
        $response = $this->put($this->getEditUserRoute($users->id), [
            'name' => $users->name,
            'email' =>  $users->email,
            'password' => $users->password,
            'confirm' => $users->password,
            'roles' => 'Admin'
        ]);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_unauth_can_not_update_user()
    {
        $users = User::factory()->create();
        $response = $this->put($this->getEditUserRoute($users->id), [
            'name' => $users->name,
            'email' =>  $users->email,
            'password' => $users->password,
            'confirm-password' => $users->password,
            'roles' => 'Admin'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    public function test_admin_user_can_see_validate_update_user()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo('user-edit');
        $users = User::factory()->create();
        $response = $this->put($this->getEditUserRoute($users->id), [
            'name' => null,
            'email' =>  $users->email,
            'password' => $users->password,
            'confirm' => $users->password,
            'roles' => 'Admin'
        ]);
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

}
