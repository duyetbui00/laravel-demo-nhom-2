<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CategoryListTest extends TestCase
{
    public function getListCategoryRoute()
    {
        return route("categories.list");
    }

    /** @test */
    public function authenticated_not_admin_can_not_view_list_categories()
    {
        $this->actingAs(User::factory()->create());
        $categories = Category::factory(1)->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_can_view_list_categories()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-list");
        $categories = Category::factory(1)->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($categories[0]->category_name);
    }

    /** @test */
    public function guest_user_cannot_view_list_categories()
    {
        $categories = Category::factory(1)->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_cannot_view_list_categories_with_search()
    {
        $categories = Category::factory(1)->create();
        $response = $this->get($this->getListCategoryRoute()."?search=".$categories[0]->category_name);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_view_list_categories_with_search()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-list");
        $categories = Category::factory(1)->create();
        $response = $this->get($this->getListCategoryRoute()."?search=".$categories[0]->category_name);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($categories[0]->category_name);
    }

    /** @test */
    public function authenticated_user_can_view_list_categories_with_pagination()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-list");
        $categories = Category::factory(3)->create();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
    }
}
