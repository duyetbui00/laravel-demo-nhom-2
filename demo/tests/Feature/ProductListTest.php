<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductListTest extends TestCase
{
    public function getProductListRoute(){
        return route("products.list");
    }

    public function test_unauth_can_not_see_list_product()
    {
        $response = $this->get($this->getProductListRoute());
        $response->assertStatus(Response::HTTP_FOUND)->assertRedirect(route("login"));
    }

    public function test_not_admin_can_not_see_list_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $response = $this->get($this->getProductListRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function test_admin_can_see_list_product()
    {
        $user=User::factory()->create();
        $admin=$this->actingAs($user);
        $user->givePermissionTo("product-list");
        $response = $this->get($this->getProductListRoute());
        $response->assertStatus(Response::HTTP_OK);
    }

}
