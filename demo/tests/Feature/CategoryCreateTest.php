<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use App\Models\User;
use App\Models\Category;

class CategoryCreateTest extends TestCase
{
    public function getViewCategoryCreateRoute()
    {
        return route('categories.create');
    }

    public function getCategoryCreateRoute()
    {
        return route('categories.store');
    }

    /** @test */
    public function unauthenticated_user_can_not_view_create_form_category()
    {
        $response = $this->get($this->getViewCategoryCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect("/login");
    }

    /** @test */
    public function auth_user_can_view_create_form_category()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-create");
        $response = $this->get($this->getViewCategoryCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function auth_not_admin_can_not_create_category()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $category = Category::factory()->make();
        $response = $this->post($this->getCategoryCreateRoute(), $category->toArray());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function auth_user_can_create_category()
    {
        $user=User::factory()->create();
        $this->actingAs($user);
        $user->givePermissionTo("category-create");
        $category = Category::factory()->make();
        $response = $this->post($this->getCategoryCreateRoute(), $category->toArray());
        $response->assertStatus(Response::HTTP_CREATED);
    }

    /** @test */
    public function auth_user_can_not_create_category_with_fields_required()
    {
        $this->withoutMiddleware();
        $this->actingAs(User::factory()->create());
        $categories = Category::factory()->make(["name" => null]);
        $response = $this->post($this->getCategoryCreateRoute(), $categories->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }

    /** @test */
    public function auth_user_can_not_create_category_with_fields_unique()
    {
        $this->withoutMiddleware();
        $this->actingAs(User::factory()->create()->givePermissionTo("category-create"));
        $categories = Category::factory()->create();
        $name = $categories->name;
        $categories = Category::factory()->make(["name" => $name]);
        $response = $this->post($this->getCategoryCreateRoute(), $categories->toArray());
        $response->assertStatus(Response::HTTP_FOUND)->assertSessionHasErrors(["name"]);
    }
}
